
require 'time'
require 'active_support/all'

class Track
	attr_reader :am_time, :lunch_time, :pm_time

	# Initializing the class and setting attributes statically. 
	def initialize
	 	@am_time = Time.parse("9:00")
		@lunch_time = Time.parse("12:00")
		@pm_time = Time.parse("13:00")
	end

	#Showing tracks
	def organize_tracks(lectures_list)
		show_tracks(lectures_list, @am_time, @lunch_time, @pm_time)
	end
end

private

# Organizing track A
def show_tracks(lectures_list, am_time, lunch_time, pm_time)
	
	# Initiating an empty hash that will receive ordened values
	am_lectures = {}
	pm_lectures = {}
	
	puts "#########################  TRACK A  ########################"
	
	#setting AM lectures
	loop do 
		am_lectures = lectures_list.to_a.combination(4).to_a.sample.to_h
		break if am_lectures.values.map.sum == 180 #min. Total time of the morning lectures
	end	

	#showing the ordened lectures for the morning period.
	am_lectures.each do |k,v|
		puts am_time.to_formatted_s(:time) << " #{k} #{v} min"
		am_time += v.minutes
	end
	puts lunch_time.to_formatted_s(:time) << " Almoço"
	
	# Subtracting the processed values from the original hash and 
	# creating a new one with the result.
	new_list = (lectures_list.to_a - am_lectures.to_a).to_h
			
	# Setting PM lectures
  loop do
		pm_lectures = new_list.to_a.combination(6).to_a.sample.to_h
		break if pm_lectures.values.map.sum == 240 #min. Total time of the afternoon lectures
	end

	# Showing the ordened lectures for the afternoon period.
  pm_lectures.each do |k,v|
   	puts pm_time.to_formatted_s(:time) << " #{k} #{v} min"
   	pm_time += v.minutes
  end
	puts pm_time.to_formatted_s(:time) << " Evento de Networking"
	
	#Subtracting the processed values from the original hash and 
	#creating a new one with the result.
  new_list = (new_list.to_a - pm_lectures.to_a).to_h

  # Calling the method for track B
  show_track_B(new_list, @am_time, @lunch_time, @pm_time)
end 	

# Organizing track B
def show_track_B(lectures_list, am_time, lunch_time, pm_time)
		
	# Initiating an empty hash that will receive ordened values
	am_lectures = {}

	puts "#########################  TRACK B  ########################"
	
	#setting AM lectures
	loop do 
		am_lectures = lectures_list.to_a.combination(4).to_a.sample.to_h
		break if am_lectures.values.map.sum == 180 #min. Total time of the morning lectures
	end	

	#showing the ordened lectures for the morning period.
	am_lectures.each do |k,v|
		puts am_time.to_formatted_s(:time) << " #{k} #{v} min"
		am_time += v.minutes
	end
	puts lunch_time.to_formatted_s(:time) << " Almoço"
	
	# Subtracting the values from the original hash and creating 
	#a new one with the result.
	new_list = (lectures_list.to_a - am_lectures.to_a).to_h
			 
  # Showing the ordened lectures for the afternoon period.
  new_list.each do |k,v|
   puts v == 5 ? pm_time.to_formatted_s(:time) << " #{k} lightning" 
   						 : pm_time.to_formatted_s(:time) << " #{k} #{v} min"
   pm_time += v.minutes
  end
	puts pm_time.to_formatted_s(:time) << " Evento de Networking"
end
