require_relative 'track'

class Event
	attr_reader :file_array, :lecture_name, :lecture_duration, :lectures_list
	
	# Initializing the class and setting the attributes
	def initialize(file_array)
		@file_array = file_array
		@lecture_name = set_lecture_name(@file_array)
		@lecture_duration = set_lecture_duration(@file_array)
		@lectures_list = set_lectures_list(@lecture_name, @lecture_duration)
	end
	
	# Showing tracks
	def show_event_tracks
		track = Track.new
		track.organize_tracks(@lectures_list)
	end
end

private
# Reducting each array by one position and setting the event name with de result.
def set_lecture_name(file_array)
	file_array.collect {|c| c.split[0..-1-1].join(' ')}
end

#collecting the last string, checking if it's lightning, getting the numbers
#from the string and converting to Integer.
def set_lecture_duration(file_array)
	file_array.collect do |c| 
	c.split.last.include?("lightning") ? c = 5 : c.split.last.gsub(/\D/,'').to_i
	end
end

#creating a hash with name and duration arrays.
def set_lectures_list(lecture_name, lecture_duration)
	Hash[lecture_name.zip (lecture_duration)]
end

 