require "event"

describe Event, 'class' do
	before(:all) do 
  	@file = File.readlines("lib/proposals.txt")
		@event = Event.new(@file)
	end
	subject {  @event  }

	context 'calls instance method' do
		it { should respond_to :show_event_tracks } 
	end

	describe '#initialize' do
		context 'set the proper instances' do
			it 'must be an Array' do
				[ @event.file_array, @event.lecture_name, @event.lecture_duration].each do |var|
					expect(var).to be_an(Array)
				end
			end
			
			it 'must be a Hash' do
				expect(@event.lectures_list).to be_a(Hash)
			end
		end
	end
end
