require "track"

describe Track, 'class' do
	before(:all) do 
  	@track = Track.new
	end
	subject {  @track  }

	context 'calls instance method' do
		it { should respond_to :organize_tracks } 
	end  

	describe 'initialize' do
		
	  context 'set the proper type' do
	  	it 'must be a Time' do
	  	  [ @track.am_time, @track.lunch_time, @track.pm_time ].each do |var|
					expect(var).to be_a(Time)
				end
			end
		end
		context 'when the time is not right' do

		  it 'must have the right time' do
			  expect(@track.am_time).to be_eql(Time.parse("9:00"))
			  expect(@track.lunch_time).to be_eql(Time.parse("12:00"))
			  expect(@track.pm_time).to be_eql(Time.parse("13:00"))
			end
		end
	end
end
